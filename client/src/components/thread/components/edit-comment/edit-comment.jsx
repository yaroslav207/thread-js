import * as React from 'react';
import PropTypes from 'prop-types';
import { ButtonType } from 'src/common/enums/enums';
import { Button, Form } from 'src/components/common/common';

const EditComment = ({ comment, editComment, onCancelEdit }) => {
  const [body, setBody] = React.useState(comment.body);

  const handleAddComment = async () => {
    if (!body) {
      return;
    }
    await editComment(comment.id, body);
    setBody('');
  };

  return (
    <Form reply onSubmit={handleAddComment}>
      <Form.TextArea
        value={body}
        placeholder="Type a comment..."
        onChange={ev => setBody(ev.target.value)}
      />
      <Button type={ButtonType.SUBMIT} isPrimary>
        Edit comment
      </Button>
      <Button type={ButtonType.BUTTON} isPrimary onClick={() => onCancelEdit()}>
        Cancel
      </Button>
    </Form>
  );
};

EditComment.propTypes = {
  editComment: PropTypes.func.isRequired,
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  onCancelEdit: PropTypes.func.isRequired
};

export default EditComment;
