import * as React from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { threadActionCreator } from 'src/store/actions';
import { Spinner, Post, Modal, Comment as CommentUI } from 'src/components/common/common';
import { useState } from 'react';
import AddComment from '../add-comment/add-comment';
import EditComment from '../edit-comment/edit-comment';
import Comment from '../comment/comment';
import { getSortedComments } from './helpers/helpers';

const ExpandedPost = ({
  sharePost
}) => {
  const [editCommentId, setEditCommentId] = useState(null);
  const dispatch = useDispatch();
  const { post } = useSelector(state => ({
    post: state.posts.expandedPost
  }));

  const handlePostLike = React.useCallback(id => (
    dispatch(threadActionCreator.likePost(id))
  ), [dispatch]);

  const handleCommentAdd = React.useCallback(commentPayload => (
    dispatch(threadActionCreator.addComment(commentPayload))
  ), [dispatch]);

  const handleExpandedPostToggle = React.useCallback(id => (
    dispatch(threadActionCreator.toggleExpandedPost(id))
  ), [dispatch]);

  const handleEditComment = React.useCallback((id, body) => (
    dispatch(threadActionCreator.updateComment(id, { body }))
  ), [dispatch]);

  const handleDeleteComment = React.useCallback(id => (
    dispatch(threadActionCreator.deleteComment(id))
  ), [dispatch]);

  const handleExpandedPostClose = () => handleExpandedPostToggle();

  const onEditComment = id => {
    setEditCommentId(id);
  };

  const onCancelEditComment = () => {
    onEditComment(null);
  };
  const sortedComments = getSortedComments(post.comments ?? []);

  const findComment = id => sortedComments.find(comment => comment.id === id);

  return (
    <Modal
      dimmer="blurring"
      centered={false}
      open
      onClose={handleExpandedPostClose}
    >
      {post ? (
        <Modal.Content>
          <Post
            post={post}
            onPostLike={handlePostLike}
            onExpandedPostToggle={handleExpandedPostToggle}
            sharePost={sharePost}
          />
          <CommentUI.Group style={{ maxWidth: '100%' }}>
            <h3>Comments</h3>
            {sortedComments.map(comment => (
              <Comment
                key={comment.id}
                comment={comment}
                onEditComment={onEditComment}
                onDeleteComment={handleDeleteComment}
              />
            ))}
            {editCommentId
              ? (
                <EditComment
                  comment={findComment(editCommentId)}
                  editComment={handleEditComment}
                  onCancelEdit={onCancelEditComment}
                />
              )
              : <AddComment postId={post.id} onCommentAdd={handleCommentAdd} />}
          </CommentUI.Group>
        </Modal.Content>
      ) : (
        <Spinner />
      )}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  sharePost: PropTypes.func.isRequired
};

export default ExpandedPost;
