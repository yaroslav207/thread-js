import * as React from 'react';
import PropTypes from 'prop-types';
import { IconName, ButtonColor, ButtonType } from 'src/common/enums/enums';
import { Modal, Button } from 'src/components/common/common';

import styles from './styles.module.scss';
import { Form, Image, Segment } from '../../../common/common';

const EditPost = ({ post, onPostEdit, uploadImage, close }) => {
  const [body, setBody] = React.useState(post.body);
  const [image, setImage] = React.useState(post.image ? { id: post.image.id, imageLink: post.image.link } : undefined);
  const [isUploading, setIsUploading] = React.useState(false);

  const handleEditPost = async () => {
    if (!body) {
      return;
    }
    await onPostEdit({ imageId: image?.imageId, body, id: post.id });
    close();
  };

  const handleUploadFile = ({ target }) => {
    setIsUploading(true);
    const [file] = target.files;

    uploadImage(file)
      .then(({ id: imageId, link: imageLink }) => {
        setImage({ imageId, imageLink });
      })
      .catch(() => {
        // TODO: show error
      })
      .finally(() => {
        setIsUploading(false);
      });
  };
  return (
    <Modal open onClose={close}>
      <Modal.Header>
        <span>Edit Post</span>
      </Modal.Header>
      <Modal.Content>
        <Segment>
          <Form onSubmit={handleEditPost}>
            <Form.TextArea
              name="body"
              value={body}
              placeholder="What is the news?"
              onChange={ev => setBody(ev.target.value)}
            />
            {image?.imageLink && (
              <div className={styles.imageWrapper}>
                <Image className={styles.image} src={image?.imageLink} alt="post" />
              </div>
            )}
            <div className={styles.btnWrapper}>
              <Button
                color="teal"
                isLoading={isUploading}
                isDisabled={isUploading}
                iconName={IconName.IMAGE}
              >
                <label className={styles.btnImgLabel}>
                  Attach image
                  <input
                    name="image"
                    type="file"
                    onChange={handleUploadFile}
                    hidden
                  />
                </label>
              </Button>
              <Button color={ButtonColor.BLUE} type={ButtonType.SUBMIT}>
                Post
              </Button>
            </div>
          </Form>
        </Segment>
      </Modal.Content>
    </Modal>
  );
};

EditPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  close: PropTypes.func.isRequired,
  onPostEdit: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired
};

export default EditPost;
