import * as React from 'react';
import PropTypes from 'prop-types';
import { getFromNowTime } from 'src/helpers/helpers';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { Comment as CommentUI, Icon } from 'src/components/common/common';
import { commentType } from 'src/common/prop-types/prop-types';

import { useSelector } from 'react-redux';
import styles from './styles.module.scss';
import { Label } from '../../../common/common';
import { IconName } from '../../../../common/enums/components/icon-name.enum';

const Comment = ({ comment: { id, body, createdAt, user }, onEditComment, onDeleteComment }) => {
  const userId = useSelector(state => (state.profile.user.id));
  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={user.image?.link ?? DEFAULT_USER_AVATAR} />
      <CommentUI.Content>
        <CommentUI.Author as="a">{user.username}</CommentUI.Author>
        <CommentUI.Metadata>{getFromNowTime(createdAt)}</CommentUI.Metadata>
        <CommentUI.Text>{body}</CommentUI.Text>
        <div>
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={() => {}}
          >
            <Icon name={IconName.THUMBS_UP} />
            {0}
          </Label>
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={() => {}}
          >
            <Icon name={IconName.THUMBS_DOWN} />
            {0}
          </Label>
          {user.id === userId
            ? (
              <div className={styles.servicePost}>
                <Label
                  basic
                  size="small"
                  as="a"
                  className={styles.toolbarBtn}
                  onClick={() => {
                    onEditComment(id);
                  }}
                >
                  <Icon name={IconName.EDIT} />
                </Label>
                <Label
                  basic
                  size="large"
                  as="a"
                  className={styles.toolbarBtn}
                  onClick={() => {
                    onDeleteComment(id);
                  }}
                >
                  <Icon name={IconName.DELETE} />
                </Label>
              </div>
            )
            : null}

        </div>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: commentType.isRequired,
  onEditComment: PropTypes.func.isRequired,
  onDeleteComment: PropTypes.func.isRequired
};

export default Comment;
