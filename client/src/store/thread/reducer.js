import { createReducer } from '@reduxjs/toolkit';
import {
  setPosts,
  addMorePosts,
  addPost,
  setExpandedPost,
  editPost,
  removePost,
  editComment,
  removeComment
} from './actions';

const initialState = {
  posts: [],
  expandedPost: null,
  hasMorePosts: true
};

const reducer = createReducer(initialState, builder => {
  builder.addCase(setPosts, (state, action) => {
    const { posts } = action.payload;

    state.posts = posts;
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(addMorePosts, (state, action) => {
    const { posts } = action.payload;

    state.posts = state.posts.concat(posts);
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(addPost, (state, action) => {
    const { post } = action.payload;

    state.posts = [post, ...state.posts];
  });
  builder.addCase(editPost, (state, action) => {
    const { post } = action.payload;

    state.posts = state.posts.map(item => (item.id === post.id ? post : item));
  });
  builder.addCase(setExpandedPost, (state, action) => {
    const { post } = action.payload;

    state.expandedPost = post;
  });
  builder.addCase(editComment, (state, action) => {
    const { post } = action.payload;

    state.expandedPost = post;
  });
  builder.addCase(removeComment, (state, action) => {
    const { post } = action.payload;
    state.expandedPost = post;
  });
  builder.addCase(removePost, (state, action) => {
    const { id } = action.payload;
    state.posts = state.posts.filter(post => post.id !== id);
  });
});

export { reducer };
