import { createAction } from '@reduxjs/toolkit';
import {
  comment as commentService,
  post as postService
} from 'src/services/services';

const ActionType = {
  ADD_POST: 'thread/add-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  EDIT_POST: 'thread/edit-post',
  REMOVE_POST: 'thread/remove-post',
  SET_EXPANDED_POST: 'thread/set-expanded-post',
  EDIT_COMMENT: 'thread/edit-comment',
  DELETE_COMMENT: 'thread/delete-comment'
};

const setPosts = createAction(ActionType.SET_ALL_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addMorePosts = createAction(ActionType.LOAD_MORE_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addPost = createAction(ActionType.ADD_POST, post => ({
  payload: {
    post
  }
}));

const editPost = createAction(ActionType.EDIT_POST, post => ({
  payload: {
    post
  }
}));

const removePost = createAction(ActionType.REMOVE_POST, id => ({
  payload: {
    id
  }
}));

const setExpandedPost = createAction(ActionType.SET_EXPANDED_POST, post => ({
  payload: {
    post
  }
}));

const editComment = createAction(ActionType.EDIT_COMMENT, comment => ({
  payload: {
    comment
  }
}));

const removeComment = createAction(ActionType.DELETE_COMMENT, post => ({
  payload: {
    post
  }
}));

const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPosts(posts));
};

const loadMorePosts = filter => async (dispatch, getRootState) => {
  const {
    posts: { posts }
  } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts.filter(
    post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
  );
  dispatch(addMorePosts(filteredPosts));
};

const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPost(post));
};

const createPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPost(newPost));
};

const updatePost = post => async dispatch => {
  const { id } = await postService.editPost({ postId: post.id, payload: post });
  const updatedPost = await postService.getPost(id);
  dispatch(editPost(updatedPost));
};

const deletePost = postId => async dispatch => {
  await postService.deletePost(postId);
  dispatch(removePost(postId));
};

const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPost(post));
};

const likePost = postId => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await postService.likePost(postId);
  const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  const isUpdate = !!createdAt && createdAt !== updatedAt;

  const mapLikes = post => ({
    ...post,
    dislikeCount: isUpdate ? Number(post.dislikeCount) - 1 : Number(post.dislikeCount),
    likeCount: Number(post.likeCount) + diff // diff is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapLikes(expandedPost)));
  }
};

const dislikePost = postId => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await postService.dislikePost(postId);
  const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  const isUpdate = !!createdAt && createdAt !== updatedAt;

  const mapDislikes = post => ({
    ...post,
    likeCount: isUpdate ? Number(post.likeCount) - 1 : Number(post.likeCount),
    dislikeCount: Number(post.dislikeCount) + diff // diff is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapDislikes(expandedPost)));
  }
};

const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

const updateComment = (commentId, body) => async (dispatch, getRootState) => {
  const { id } = await commentService.editComment(commentId, body);
  const updatedComment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    comments: post.comments.map(comment => ((comment.id === updatedComment.id)
      ? updatedComment
      : comment)) // comment is taken from the current closure
  });

  const {
    posts: { expandedPost }
  } = getRootState();

  if (expandedPost && expandedPost.id === updatedComment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

const deleteComment = commentId => async (dispatch, getRootState) => {
  const { id } = await commentService.deleteComment(commentId);
  console.log(id);
  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) - 1,
    comments: post.comments.filter(comment => comment.id !== id)// comment is taken from the current closure
  });

  const {
    posts: { expandedPost }
  } = getRootState();

  if (expandedPost && id) {
    dispatch(removeComment(mapComments(expandedPost)));
  }
};

export {
  setPosts,
  addMorePosts,
  addPost,
  editPost,
  setExpandedPost,
  removePost,
  editComment,
  removeComment,
  loadPosts,
  deleteComment,
  loadMorePosts,
  applyPost,
  createPost,
  updatePost,
  deletePost,
  toggleExpandedPost,
  likePost,
  addComment,
  dislikePost,
  updateComment
};
