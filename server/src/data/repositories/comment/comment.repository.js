import { Abstract } from '../abstract/abstract.repository';
import { sequelize } from '../../db/connection';

const likeCase = bool => `CASE WHEN "commentReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class Comment extends Abstract {
  constructor({ commentModel, userModel, imageModel }) {
    super(commentModel);
    this._userModel = userModel;
    this._imageModel = imageModel;
  }

  async getComments() {
    const where = { deletedAt: null };

    return this.model.findAll({
      where,
      attributes: {
        include: [
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: this._userModel,
        attributes: ['id', 'username']
      }],
      group: [
        'post.id',
        'user.id'
      ],
      order: [['createdAt', 'DESC']]
    });
  }

  getCommentById(id) {
    return this.model.findOne({
      group: ['comment.id', 'user.id', 'user->image.id'],
      where: { id },
      include: [
        {
          model: this._userModel,
          attributes: ['id', 'username'],
          include: {
            model: this._imageModel,
            attributes: ['id', 'link']
          }
        }
      ]
    });
  }
}

export { Comment };
