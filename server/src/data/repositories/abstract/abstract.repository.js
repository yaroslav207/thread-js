class Abstract {
  constructor(model) {
    this.model = model;
  }

  async getAll() {
    const result = await this.model.findAll();
    return result;
  }

  getById(id) {
    return this.model.findByPk(id);
  }

  create(data) {
    return this.model.create(data);
  }

  async updateById(id, data) {
    const result = await this.model.update(data, {
      where: { id },
      returning: true,
      plain: true
    });

    return result[1];
  }

  async softDelete(id) {
    const result = await this.updateById(
      id,
      {
        deletedAt: Date.now()
      }
    );
    return result.dataValues;
  }

  deleteById(id) {
    return this.model.destroy({
      where: { id }
    });
  }
}

export { Abstract };
