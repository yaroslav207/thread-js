class Comment {
  constructor({ commentRepository }) {
    this._commentRepository = commentRepository;
  }

  create(userId, comment) {
    return this._commentRepository.create({
      ...comment,
      userId
    });
  }

  getCommentById(id) {
    return this._commentRepository.getCommentById(id);
  }

  update(commentId, comment) {
    return this._commentRepository.updateById(commentId, comment);
  }

  delete(commentId) {
    return this._commentRepository.softDelete(commentId);
  }
}

export { Comment };
