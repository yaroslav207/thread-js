import { comment } from '../../services/services';

const permissionsComment = async (req, res, next) => {
  const result = await comment.getCommentById(req.params.id);
  if (result.userId === req.user.id) {
    next();
  } else {
    res.status(403).send('Forbidden');
  }
};

export { permissionsComment };
