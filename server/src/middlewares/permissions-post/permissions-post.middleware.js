import { post } from '../../services/services';

const permissionsPost = async (req, res, next) => {
  const result = await post.getCommentById(req.params.id);
  if (result.userId === req.user.id) {
    next();
  } else {
    res.status(403).send('Forbidden');
  }
};

export { permissionsPost };
